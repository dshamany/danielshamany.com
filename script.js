const THEME = 'currentTheme'; // this is the eact value in localStorage
const THEME_TYPE = ['dark', 'light'];
let currentTheme = localStorage.getItem(THEME);
let themeBtn = document.getElementById('theme-btn');
let style = document.getElementById('page-style');

// Menu
function toggleMenu() {
    let menu = document.getElementById('menu');
    if (menu.className === 'menu') {
        menu.className = 'menu-open';
    } else {
        menu.className = 'menu';
    }
}

function loadPage(anchor) {
    document.location.href = '#' + anchor;
    toggleMenu(); // close menu when button is selected
}

function toggleStyle() {
    let currentTheme = localStorage.getItem(THEME);
    if (currentTheme === THEME_TYPE[0]) {
        localStorage.setItem(THEME, THEME_TYPE[1]);
    } else {
        localStorage.setItem(THEME, THEME_TYPE[0]);
    }
    currentTheme = localStorage.getItem(THEME);
    style.setAttribute('href', `./defaults-${currentTheme}.css`);
    themeBtn.innerHTML = (currentTheme === THEME_TYPE[1] ? THEME_TYPE[0] : THEME_TYPE[1]) + ' Theme';
    toggleMenu();
}


// Initialize site with variables to localStorage
function init() {
    localStorage.setItem(THEME, THEME_TYPE[1]);
    // update style
    style.setAttribute('href', `./defaults-${THEME_TYPE[1]}.css`);
    themeBtn.innerHTML = THEME_TYPE[0] + ' Theme';
}

// Initialize only if the user used the site for the first time
if (!currentTheme) {
    init();
} else {
    currentTheme = localStorage.getItem(THEME);
    style.setAttribute('href', `./defaults-${currentTheme}.css`);
    themeBtn.innerHTML = (currentTheme === THEME_TYPE[1] ? THEME_TYPE[0] : THEME_TYPE[1]) + ' Theme';
}